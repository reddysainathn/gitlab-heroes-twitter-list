package main

import (
	"log"
	"net/http"
	"os"
	"path"
	"regexp"

	"github.com/PuerkitoBio/goquery"
	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
)

func main() {

	// Request the HTML page.
	res, err := http.Get("https://about.gitlab.com/community/heroes/members/")
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	var slugs []string
	// Find the review items
	doc.Find(".social").Each(func(i int, s *goquery.Selection) {
		// For each item found, get the band and title
		res, _ := s.Find("a").Attr("href")

		matched, err := regexp.MatchString(`^https://twitter.com.*`, res)
		if err != nil {
			log.Fatalln(err)
		}

		if matched {
			// only use the last part of the URL = the Twitter handle
			slugs = append(slugs, path.Base(res))
		}

	})

	config := oauth1.NewConfig(os.Getenv("CONSUMERKEY"), os.Getenv("CONSUMERSECRET"))
	token := oauth1.NewToken(os.Getenv("ACCESSTOKEN"), os.Getenv("ACCESSSECRET"))
	httpClient := config.Client(oauth1.NoContext, token)

	// Twitter client
	client := twitter.NewClient(httpClient)

	// Add member to list
	debug := os.Getenv("DEBUG")
	for _, slug := range slugs {

		if debug == "false" {
			resp, err := client.Lists.MembersCreate(&twitter.ListsMembersCreateParams{ListID: 1221901753425309698, ScreenName: slug})

			if err != nil {
				log.Fatalln(err)
			}

			log.Printf("Slug: %s - Response: %s", slug, resp.Status)

		} else {
			log.Printf("Debug slug: %s", slug)
			resp, _ := client.Lists.MembersCreate(&twitter.ListsMembersCreateParams{ListID: 1221901753425309698, ScreenName: "m4r10k"})
			log.Println(resp)
			break
		}

	}

}
